# Unbound, a validating, recursive, caching DNS resolver.
Developed by [NLnet Labs](https://nlnetlabs.nl/), [Unbound](https://nlnetlabs.nl/projects/unbound/about/) is a very secure DNS resolver that offers validation, recursion and caching. Unbound's design is a set of modular components which includes features such as [DNSCrypt](https://www.dnscrypt.org/), [DNSSEC](https://www.verisign.com/en_US/domain-names/dnssec/index.xhtml) and [Query Name Minimisation](https://tools.ietf.org/html/rfc7816).

The installation and configuration of Unbound is made to be very simple by design and most operating systems today already have Unbound packages availble, and it currently runs on many systems - such as *BSDs, Linux and Microsoft Windows.
Although Unbound is not designed to be a complete standalone authoritative server, you can configure A records for lookups for a small LAN. It's recommended, however, to set up an [NSD server](https://nlnetlabs.nl/projects/nsd/about/), also developed by NLnet Labs, as an authoritative solution together with Unbound and it can be done in the same device if so you wish.

A very interesting feature in Unbound is that unlike some other resolvers, if you don't use forwarders for requests, it queries root servers directly using an IP address register that is listed in the [Root Hints File](https://www.iana.org/domains/root/files)

In the future, It's expected that many Open Source distributions will move away from BIND to Unbound, and both on OpenBSD and FreeBSD, BIND is no longer included in the default installation.
## Pre-setup
In this tutorial, Unbound will be configured with DNSSEC and DoT (DNS over TLS) enabled, a pair of NSD servers to query local addresses, and forwarders for addresses that are outside my local network. We will also provide an anti-ad file to try blocking most of them by spoofing them to localhost.

My local network have the following subnets: 192.168.0.0/24, 10.1.1.0/24 and 10.2.2.0/24

Lastly, as the titles implies - this guide was written using OpenBSD 6.7 but you can use any OS you would like, just keeping in mind the different directory paths and set of commands.
## Setting Unbound up
Unbound is already present in the default installation of OpenBSD, so install it if you're running any other OS and it doesn't come pre-installed as default.

I'll be making two directories - log and run because I will run Unbound chrooted. Although we have the option to leave the pidfile in the default place (/var/run/), I prefer all the files to be in the same directory.
About the log, we could use syslog, but I'd rather leave the log in Unbound's directory as well. So we're creating both directories and setting their permissions:
```
$ doas mkdir /var/unbound/{run,log}
$ doas chown root:_unbound /var/unbound/{run,log}
$ doas chmod 755 /var/unbound/{run,log}
```
We can enable Unbound as we have a preset .conf file already supplied and we can use it to test if it's working. Also change your resolv.conf so you can query Unbound on the localhost:
```
$ echo "nameserver 127.0.0.1" | doas tee /etc/resolv.conf
$ doas rcctl enable unbound
$ doas rcctl start unbound
```
Now we can see if it's working with dig or nslookup, or any other tool you'd like. Note that you might not have those tools installed and if so, you may get these with the bind-utils package. If the results are positive, you will see an output similar as mine, with the localhost queried in the SERVER section, and status: NOERROR. This means that Unbound is already working.
```
$ dig www.openbsd.org

; <<>> dig 9.10.8-P1 <<>> www.openbsd.org
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 64064
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;www.openbsd.org.               IN      A

;; ANSWER SECTION:
www.openbsd.org.        28800   IN      A       129.128.5.194

;; Query time: 2936 msec
;; SERVER: 127.0.0.1#53(127.0.0.1)
;; WHEN: Tue Nov 10 15:10:01 -03 2020
;; MSG SIZE  rcvd: 60
```
Now we have to start setting up our own conf. Since we are going to enable DNSSEC, we have to obtain a KSK (Root Key Signing Key). For that we can use the following command:
```
$ doas unbound-anchor -a "/var/unbound/db/root.key"
```
We will also get an already built list of adservers so we can include it in our .conf file in order to try blocking some content. There are many online sources and you can run a simple shell script to get these files, concatenate them and convert them into a list for Unbound for blocking ads, porn, trackers and whatnot. Here, we'll be using just a single file and moving it to Unbound's dir:
```
$ curl -sS -L --compressed "http://pgl.yoyo.org/adservers/serverlist.php?hostformat=unbound&showintro=0&mimetype=plaintext" > unbound-adfilter.conf
$ doas mv unbound-adfilter.conf /var/unbound/etc/unbound-adfilter.conf
```
Now we can get to edit the .conf file. Rename the preset .conf file if you'd like and create another using your favourite editor:
```
$ doas mv /var/unbound/etc/unbound.conf /var/unbound/etc/.unbound.conf.backup
$ doas vim /var/unbound/etc/unbound.conf
```
```
# $OpenBSD: unbound.conf,v 1.19 2019/11/07 15:46:37 sthen Exp $

server:
        interface: 127.0.0.1                                           # Interfaces Unbound will be listening on;
        interface: 10.1.1.127
        access-control: 127.0.0.0/8 allow                              # Allows access to the listed netblocks;
        access-control: 10.0.0.0/8 allow
        access-control: 192.168.0.0/24 allow
        port: 53                                                       # Port Unbond will be listening on;
        do-ip4: yes                                                    # Allow IPv4;
        do-ip6: no                                                     # Refuse IPv6;
        do-udp: yes                                                    # Enables TCP queries to be answered/issued;
        do-tcp: yes                                                    # Enables UDP queries to be answered/issued;

        directory: "/var/unbound"                                      # Working directory;
        chroot: "/var/unbound"                                         # Chroot directory;
        username: "_unbound"                                           # User to be bound to port. Drops user privileges after binding;
        auto-trust-anchor-file: "/var/unbound/db/root.key"             # Trust anchor file. Enables DNSSEC;
        tls-cert-bundle: "/etc/ssl/cert.pem"                           # Cert file to auth connections. Enables DoT connections;

        pidfile: "/var/unbound/run/unbound.pid"                        # Pidfile;
        logfile: "/var/unbound/log/unbound.log"                        # Logfile;
        verbosity: 5                                                   # Set log verbosity;
        use-syslog: no                                                 # Set to no if using log options;
        log-time-ascii: yes                                            # Sets loglines to use timestamp in UTC ascii;
        log-queries: yes                                               # Prints one line per query. Makes the server slower;
        log-replies: yes                                               # Prints one line per reply. Makes the server slower;

        hide-identity: yes                                             # Refuses id.server and hostname.bind queries;
        identity: ""                                                   # Identity to report. "" sets to hostname;
        hide-version: yes                                              # Refuses version.server and version.bind queries;
        version: ""                                                    # Version to report. "" sets to package version;

        hide-trustanchor: yes                                          # Refuses trustanchor.unbound queries;
        harden-glue: yes                                               # Trust glue only if's within server's authority;
        harden-dnssec-stripped: yes                                    # Requires DNSSEC for trust-anchored zones. If data is absent, zone becomes bogus;
        aggressive-nsec: yes                                           # Helps reducing query rates towards targets that get NXDOMAIN constantly;
        qname-minimisation: yes                                        # Send minimum info to upstream servers to enhance privacy;
        use-caps-for-id: yes                                           # Uses 0x20-encoded random bits in query to foil spoof attempts;
        val-clean-additional: yes                                      # Removes data from additional section that are not signed properly;
        
        include: "/var/unbound/etc/unbound-adfilter.conf"              # Includes ad-blocker;

        prefetch: yes                                                  # Keep cache up to date prefetching cached elements before they expire;
        num-threads: 1                                                 # Number of threads to create. Use 1 for no threading;
        cache-max-ttl: 86400                                           # Max TTL for RRsets and messages in the cache;
        cache-min-ttl: 1200                                            # Min TTL. More than one hour could make the data stale;
        
        unwanted-reply-threshold: 10000                                # Issues warning and defensive action is taken if the threshold is reached;
        do-not-query-localhost: no                                     # Set no for testing, so you allow the resolver to send queries to localhost;

        private-address: 10.0.0.0/8                                    # Refuses private subnets from being returned for public internet names;
        private-address: 192.168.0.0/16
        private-domain: "example.net"                                  # Allows this domain to contain private addresses;
        
        local-zone: "168.192.in-addr.arpa." nodefault                  # Sets subnets as local, as to query the NSD servers for the IPs and hostnames;
        local-zone: "10.in-addr.arpa." nodefault

# -------------------------------------------------
        # Stub Zones can be used to configure authoritative data to be used by the resolver that cannot be accessed using the pub internet servers.
        # These zones makes Unbound query our NSD servers for local addresses.

stub-zone:
        name: "example.net"
        stub-addr: 10.2.2.112@53530
        stub-addr: 10.2.2.224@53530
stub-zone:
        name: "10.in-addr.arpa."
        stub-addr: 10.2.2.112@53530
        stub-addr: 10.2.2.224@53530
stub-zone:
        name: "168.192.in-addr.arpa."
        stub-addr: 10.2.2.112@53530
        stub-addr: 10.2.2.224@53530

# -------------------------------------------------
        # Zones we will use to query addresses that are not in our local network.
        # We will be using port 853 in order to allow DoT.
        
forward-zone:
        name: "."
        forward-ssl-upstream: yes
        forward-addr: 176.9.93.198@853#dnsforge.de
        forward-addr: 91.239.100.100@853#unicast.uncensoreddns.org
        
# -------------------------------------------------
```
Now we can reload Unbound and test if it's working, quering both locally and the Internet. We will also test if DNSSEC is working.
```
$ doas rcctl restart unbound
$ dig com. SOA +dnssec

; <<>> dig 9.10.8-P1 <<>> com. SOA +dnssec
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 63180
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags: do; udp: 4096
;; QUESTION SECTION:
;com.                           IN      SOA

;; ANSWER SECTION:
com.                    1200    IN      SOA     a.gtld-servers.net. nstld.verisign-grs.com. 1605036535 1800 900 604800 86400
com.                    1200    IN      RRSIG   SOA 8 1 900 20201117192855 20201110181855 31510 com. CSZ18uLjrUSx+699tyUCFqDE4nDHQQ34QD+k358jooHuDugW6vuR1MVl AE6uLLf8oN3rMuGn+FgBSOd+DwNZteY8VmuTMpINyYZmMaPvdfOU2BpZ vC2DU3hLYIuOPH8Ojl/dDULK63bl8RojJuv7CvXWVyGb26MFQAskbuOu 1gDFjN0IiQJuE9TqJ5QxVh1jbFKpoYcczQWaBZCp7KdvAQ==

;; Query time: 926 msec
;; SERVER: 127.0.0.1#53(127.0.0.1)
;; WHEN: Tue Nov 10 16:29:25 -03 2020
;; MSG SIZE  rcvd: 300
```
The flag ad (Authenticated Data) shows that DNSSEC is working correctly. Now I'll query my NSD slave server in order to check if the sutb-zones are working as well:
```
$ dig ns02.example.net

; <<>> dig 9.10.8-P1 <<>> ns02.example.net
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 36589
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;ns02.example.net.                 IN      A

;; ANSWER SECTION:
ns02.example.net.         1800    IN      A       10.2.2.224

;; Query time: 2932 msec
;; SERVER: 127.0.0.1#53(127.0.0.1)
;; WHEN: Tue Nov 10 16:33:12 -03 2020
;; MSG SIZE  rcvd: 58
```
And lastly, I will query a domain over the Internet that has to be made through the forwarder I've configured with DoT, through port 853:
```
$ dig www.openbsd.org

; <<>> dig 9.10.8-P1 <<>> www.openbsd.org
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 48299
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;www.openbsd.org.               IN      A

;; ANSWER SECTION:
www.openbsd.org.        28800   IN      A       129.128.5.194

;; Query time: 4232 msec
;; SERVER: 127.0.0.1#53(127.0.0.1)
;; WHEN: Tue Nov 10 16:36:13 -03 2020
;; MSG SIZE  rcvd: 60
```
And we can verify that the query was done as we configured by taking a look at the log:
```
$ doas tail /var/unbound/log/unbound.log


Nov 21 17:20:40 unbound[98961:0] info: sending query: openbsd.org. A IN
Nov 21 17:20:40 unbound[98961:0] debug: sending to target: <.> 176.9.93.198#853
Nov 21 17:20:40 unbound[98961:0] debug: qname perturbed to openbSd.oRg.
Nov 21 17:20:40 unbound[98961:0] debug: the query is using TLS encryption, for dnsforge.de
Nov 21 17:20:40 unbound[98961:0] debug: SSL connection authenticated ip4 176.9.93.198 port 853 (len 16)
```
And that's all we need for Unbound. Now you have a highly-secure ad-blocking DNS server running Unbound with DNSSEC and DoT. As said earlier, instead of querying another NSD server for our local devices, we could set records in the .conf file or set up NSD in the same machine listening for localhost on another port.

There are many options you can choose with Unbound, especially regarding security and optimization. I hope this guide helps providing the basics for any setup you would like. As always, I highly recommend you to read [unbound(8)](https://man.openbsd.org/unbound) and [unbound.conf(5)](https://man.openbsd.org/unbound.conf) in order to understand better each section and option before adjusting settings for your own needs.

## Final considerations
I have an [NSD tutorial](https://codeberg.org/cgoslaw/nsd) that can be used with this one to provide a complete DNS solution. Check it out if you're interested.

If you have any comments, corrections or suggestions you consider appropriate, please feel free to contact me.
