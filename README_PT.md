# Unbound, um DNS resolver com validação, recursão e cache

Desenvolvido pela [NLnet Labs](https://nlnetlabs.nl/), o [Unbound](https://nlnetlabs.nl/projects/unbound/about/) é um DNS resolver seguro, que oferece opções de recursão, validação, e cache. O design do Unbound é feito em cima de componentes modulares que incluem funções como [DNSCrypt](https://www.dnscrypt.org/), [DNSSEC](https://www.verisign.com/en_US/domain-names/dnssec/index.xhtml) e [Query Name Minimisation](https://tools.ietf.org/html/rfc7816).

A configuração do Unbound foi feita de modo que pudesse ser bem simples e boa parte dos sistemas operacionais modernos já tem pacotes disponíveis do Unbound em seus repositórios. No momento, o Unbound funciona em diversos sistemas como nos *BSDs, Linux e até no Microsoft Windows.
Por mais que o Unbound não seja um servidor autoritativo completo, é possível fazer configuração de registros A (Address Registers) para lookups em uma LAN. É recomendado, entretanto, que um servidor [NSD](https://nlnetlabs.nl/projects/nsd/about/), também desenvolvido pela NLnet Labs, seja configurado como uma solução autoritativa junto do Unbound, podendo ser feito até na mesma máquina.

Uma função interessante do Unbound, é que diferente de alguns outros resolvers, caso você não utilize forwarders para requisições, o Unbound realiza as requisições diretamente nos root servers usando um registro de endereços de IP listados no [Root Hints File](https://www.iana.org/domains/root/files).

No futuro, é esperado que diversas distribuições Open Source trocarão o BIND para o Unbound, e tanto no OpenBSD quanto no FreeBSD, o BIND já não está mais incluso nas instalações padrões.
## Pre-setup
Neste guia, Unbound será configurado com DNSSEC e DoT (DNS over TLS) habilitado, um par de servidores NSD para consultas locais e direcionadores para endereços fora da rede local. Nós também vamos configurar um arquivo anti-ad para tentar bloquear a maioria deles, fazendo spoofing destes ao nosso localhost.

Minha rede local tem as seguintes subnets: 192.168.0.0/24, 10.1.1.0/24 e 10.2.2.0/24.

Por fim, como o título implica - este guia foi escrito utilizando OpenBSD 6.7, mas é possível utilizar qualquer sistema que desejar, apenas tendo em mente os diretórios do software que podem ser diferentes e alguns comandos que serão usados aqui.
## Configurando o Unbound
O Unbound já está presente na instalação padrão do OpenBSD, então instale caso você esteja utilizando outro OS onde o Unbound não venha instalado por padrão.

Criarei dois diretórios - log e run, porque eu irei rodar o Unbound chrooted. Por mais que tenhamos a opção de deixar o pidfile no seu diretório padrão (/var/run/), eu prefiro deixar todos os arquivos no mesmo local.
Sobre o log, poderiamos usar o syslog, mas prefiro também deixar no mesmo local. Então criarei ambos os diretórios e arrumarei suas permissões:
```
$ doas mkdir /var/unbound/{run,log}
$ doas chown root:_unbound /var/unbound/{run,log}
$ doas chmod 755 /var/unbound/{run,log}
```
Nós podemos habilitar o Unbound já que já temos um arquivo .conf de exemplo, e podemos usá-lo para testar se está funcionando corretamente. Também, modifique seu /etc/resolv.conf de modo a realizar queries ao Unbound no localhost:
```
$ echo "nameserver 127.0.0.1" | doas tee /etc/resolv.conf
$ doas rcctl enable unbound
$ doas rcctl start unbound
```
Agora podemos ver se está funcionando com [dig](https://man.openbsd.org/dig) ou [nslookup](https://man.openbsd.org/nslookup), ou qualquer outra ferramenta de sua preferência. Note que você pode não ter essas ferramentas instaladas e caso esta situação se aplique, você pode obter estas ferramentas com o pacote bind-utils. Caso os resultados dos testes sejam positivos, você verá um output similar ao meu, com o localhost consultado na seção SERVER, e o status de NOERROR. 
Isto significa que o Unbound já está funcionando.
```
$ dig www.openbsd.org

; <<>> dig 9.10.8-P1 <<>> www.openbsd.org
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 64064
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;www.openbsd.org.               IN      A

;; ANSWER SECTION:
www.openbsd.org.        28800   IN      A       129.128.5.194

;; Query time: 2936 msec
;; SERVER: 127.0.0.1#53(127.0.0.1)
;; WHEN: Tue Nov 10 15:10:01 -03 2020
;; MSG SIZE  rcvd: 60
```
Agora temos de iniciar a nossa própria configuração. Já que vamos utilizar DNSSEC, precisamos obter um KSK (Root Key Signing Key). Para isto, usaremos o seguinte comando:
```
$ doas unbound-anchor -a "/var/unbound/db/root.key"
```
Nós também pegaremos uma lista já pronta com adservers de modo que possamos incluir no nosso arquivo .conf para que possamos bloquear alguns conteúdos de propaganda. Há diversas fontes online e é possível rodar um shell script simples para pegar estes arquivos, concatená-los e convertê-los em uma lista para o Unbound, de modo que bloqueie ads, sites de conteúdo adulto, trackers, etc. Aqui, estaremos usando apenas um arquivo único e o moveremos para o diretório do Unbound:
```
$ curl -sS -L --compressed "http://pgl.yoyo.org/adservers/serverlist.php?hostformat=unbound&showintro=0&mimetype=plaintext" > unbound-adfilter.conf
$ doas mv unbound-adfilter.conf /var/unbound/etc/
```
Agora podemos começar nosso arquivo .conf. Renomeie o arquivo padrão caso queira e crie outro usando seu editor preferido:
```
$ doas mv /var/unbound/etc/unbound.conf /var/unbound/etc/.unbound.conf.backup
$ doas vim /var/unbound/etc/unbound.conf
```
```
# $OpenBSD: unbound.conf,v 1.19 2019/11/07 15:46:37 sthen Exp $

server:
        interface: 127.0.0.1                                           # Interfaces Unbound will be listening on;
        interface: 10.1.1.127
        access-control: 127.0.0.0/8 allow                              # Allows access to the listed netblocks;
        access-control: 10.0.0.0/8 allow
        access-control: 192.168.0.0/24 allow
        port: 53                                                       # Port Unbond will be listening on;
        do-ip4: yes                                                    # Allow IPv4;
        do-ip6: no                                                     # Refuse IPv6;
        do-udp: yes                                                    # Enables TCP queries to be answered/issued;
        do-tcp: yes                                                    # Enables UDP queries to be answered/issued;

        directory: "/var/unbound"                                      # Working directory;
        chroot: "/var/unbound"                                         # Chroot directory;
        username: "_unbound"                                           # User to be bound to port. Drops user privileges after binding;
        auto-trust-anchor-file: "/var/unbound/db/root.key"             # Trust anchor file. Enables DNSSEC;
        tls-cert-bundle: "/etc/ssl/cert.pem"                           # Cert file to auth connections. Enables DoT connections;

        pidfile: "/var/unbound/run/unbound.pid"                        # Pidfile;
        logfile: "/var/unbound/log/unbound.log"                        # Logfile;
        verbosity: 5                                                   # Set log verbosity;
        use-syslog: no                                                 # Set to no if using log options;
        log-time-ascii: yes                                            # Sets loglines to use timestamp in UTC ascii;
        log-queries: yes                                               # Prints one line per query. Makes the server slower;
        log-replies: yes                                               # Prints one line per reply. Makes the server slower;

        hide-identity: yes                                             # Refuses id.server and hostname.bind queries;
        identity: ""                                                   # Identity to report. "" sets to hostname;
        hide-version: yes                                              # Refuses version.server and version.bind queries;
        version: ""                                                    # Version to report. "" sets to package version;

        hide-trustanchor: yes                                          # Refuses trustanchor.unbound queries;
        harden-glue: yes                                               # Trust glue only if's within server's authority;
        harden-dnssec-stripped: yes                                    # Requires DNSSEC for trust-anchored zones. If data is absent, zone becomes bogus;
        aggressive-nsec: yes                                           # Helps reducing query rates towards targets that get NXDOMAIN constantly;
        qname-minimisation: yes                                        # Send minimum info to upstream servers to enhance privacy;
        use-caps-for-id: yes                                           # Uses 0x20-encoded random bits in query to foil spoof attempts;
        val-clean-additional: yes                                      # Removes data from additional section that are not signed properly;
        
        include: "/var/unbound/etc/unbound-adfilter.conf"              # Includes ad-blocker;

        prefetch: yes                                                  # Keep cache up to date prefetching cached elements before they expire;
        num-threads: 1                                                 # Number of threads to create. Use 1 for no threading;
        cache-max-ttl: 86400                                           # Max TTL for RRsets and messages in the cache;
        cache-min-ttl: 1200                                            # Min TTL. More than one hour could make the data stale;
        
        unwanted-reply-threshold: 10000                                # Issues warning and defensive action is taken if the threshold is reached;
        do-not-query-localhost: no                                     # Set no for testing, so you allow the resolver to send queries to localhost;

        private-address: 10.0.0.0/8                                    # Refuses private subnets from being returned for public internet names;
        private-address: 192.168.0.0/16
        private-domain: "example.net"                                  # Allows this domain to contain private addresses;
        
        local-zone: "168.192.in-addr.arpa." nodefault                  # Sets subnets as local, as to query the NSD servers for the IPs and hostnames;
        local-zone: "10.in-addr.arpa." nodefault

# -------------------------------------------------
        # Stub Zones can be used to configure authoritative data to be used by the resolver that cannot be accessed using the pub internet servers.
        # These zones makes Unbound query our NSD servers for local addresses.

stub-zone:
        name: "example.net"
        stub-addr: 10.2.2.112@53530
        stub-addr: 10.2.2.224@53530
stub-zone:
        name: "10.in-addr.arpa."
        stub-addr: 10.2.2.112@53530
        stub-addr: 10.2.2.224@53530
stub-zone:
        name: "168.192.in-addr.arpa."
        stub-addr: 10.2.2.112@53530
        stub-addr: 10.2.2.224@53530

# -------------------------------------------------
        # Zones we will use to query addresses that are not in our local network.
        # We will be using port 853 in order to allow DoT.
        
forward-zone:
        name: "."
        forward-ssl-upstream: yes
        forward-addr: 176.9.93.198@853#dnsforge.de
        forward-addr: 91.239.100.100@853#unicast.uncensoreddns.org
        
# -------------------------------------------------
```
Agora podemos fazer um reload no Unbound e testar se está funcionando, fazendo consultas tanto locais quanto na Internet. Testaremos também se o DNSSEC está funcionando.
```
# -------------------------------------------------
$ doas rcctl restart unbound
$ dig com. SOA +dnssec

; <<>> dig 9.10.8-P1 <<>> com. SOA +dnssec
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 63180
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags: do; udp: 4096
;; QUESTION SECTION:
;com.                           IN      SOA

;; ANSWER SECTION:
com.                    1200    IN      SOA     a.gtld-servers.net. nstld.verisign-grs.com. 1605036535 1800 900 604800 86400
com.                    1200    IN      RRSIG   SOA 8 1 900 20201117192855 20201110181855 31510 com. CSZ18uLjrUSx+699tyUCFqDE4nDHQQ34QD+k358jooHuDugW6vuR1MVl AE6uLLf8oN3rMuGn+FgBSOd+DwNZteY8VmuTMpINyYZmMaPvdfOU2BpZ vC2DU3hLYIuOPH8Ojl/dDULK63bl8RojJuv7CvXWVyGb26MFQAskbuOu 1gDFjN0IiQJuE9TqJ5QxVh1jbFKpoYcczQWaBZCp7KdvAQ==

;; Query time: 926 msec
;; SERVER: 127.0.0.1#53(127.0.0.1)
;; WHEN: Tue Nov 10 16:29:25 -03 2020
;; MSG SIZE  rcvd: 300
```
A flag ad (Authenticated Data) demonstra que o DNSSEC está funcionando corretamente. Agora uma query local do meu servidor NSD slave para confirmar que as queries para nossa stub-zone local também estão funcionando: 
```
$ dig ns02.example.net

; <<>> dig 9.10.8-P1 <<>> ns02.example.net
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 36589
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;ns02.example.net.                 IN      A

;; ANSWER SECTION:
ns02.example.net.         1800    IN      A       10.2.2.224

;; Query time: 2932 msec
;; SERVER: 127.0.0.1#53(127.0.0.1)
;; WHEN: Tue Nov 10 16:33:12 -03 2020
;; MSG SIZE  rcvd: 58
```
E por fim, farei uma query de um domain na Internet que terá de ser feito pelo forwarder que configurei com DoT, pela porta 853:
```
$ dig www.openbsd.org

; <<>> dig 9.10.8-P1 <<>> www.openbsd.org
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 48299
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;www.openbsd.org.               IN      A

;; ANSWER SECTION:
www.openbsd.org.        28800   IN      A       129.128.5.194

;; Query time: 4232 msec
;; SERVER: 127.0.0.1#53(127.0.0.1)
;; WHEN: Tue Nov 10 16:36:13 -03 2020
;; MSG SIZE  rcvd: 60

# -------------------------------------------------
```
E podemos confirmar que a query foi feita para nosso forwader com DoT através do log:
```
$ doas tail /var/unbound/log/unbound.log


Nov 21 17:20:40 unbound[98961:0] info: sending query: openbsd.org. A IN
Nov 21 17:20:40 unbound[98961:0] debug: sending to target: <.> 176.9.93.198#853
Nov 21 17:20:40 unbound[98961:0] debug: qname perturbed to openbSd.oRg.
Nov 21 17:20:40 unbound[98961:0] debug: the query is using TLS encryption, for dnsforge.de
Nov 21 17:20:40 unbound[98961:0] debug: SSL connection authenticated ip4 176.9.93.198 port 853 (len 16)
```
E tudo pronto com o Unbound. Agora você tem um servidor DNS altamente-seguro, com ad-blocking, executando Unbound com DNSSEC e DoT. Como dito previamente, ao invés de consultar outro servidor NSD para nossas máquinas locais, podemos definir registros no próprio arquivo .conf ou até subir um NSD na própria máquina listening no localhost em outra porta.

Há diversas opções que você pode escolher com Unbound, especialmente referentes à segurança e optimização. Espero que este guia ajude a oferecer o básico a qualquer configuração que deseje. Como sempre, recomendo que leia os manuais [unbound(8)](https://man.openbsd.org/unbound) e [unbound.conf(5)](https://man.openbsd.org/unbound.conf) de modo a entender melhor cada seção e opções antes de ajustá-las para suas necessidades.

## Considerações finais
Eu tenho também um [tutorial de NSD](https://codeberg.org/cgoslaw/nsd) que pode ser usado como um complemento a este para que você possa obter uma solução completa de DNS. Dêem uma olhada caso tenham interesse.

Em caso de comentários, correções ou sugestões que considere apropriado, por favor sinta-se livre em me contatar.
